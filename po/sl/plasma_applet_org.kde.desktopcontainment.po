# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Andrej Mernik <andrejm@ubuntu.si>, 2014, 2015, 2016, 2017, 2018.
# Matjaž Jeran <matjaz.jeran@amis.net>, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-05 02:00+0000\n"
"PO-Revision-Date: 2023-08-01 10:13+0200\n"
"Last-Translator: Matjaž Jeran <matjaz.jeran@amis.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"Translator: Andrej Mernik <andrejm@ubuntu.si>\n"
"X-Generator: Poedit 3.3.2\n"

#: package/contents/config/config.qml:16
#, kde-format
msgid "Location"
msgstr "Mesto"

#: package/contents/config/config.qml:23
#: package/contents/ui/FolderViewLayer.qml:394
#, kde-format
msgid "Icons"
msgstr "Ikone"

#: package/contents/config/config.qml:30
#, kde-format
msgid "Filter"
msgstr "Filter"

#: package/contents/ui/BackButtonItem.qml:104
#, kde-format
msgid "Back"
msgstr "Nazaj"

#: package/contents/ui/ConfigFilter.qml:63
#, kde-format
msgid "Files:"
msgstr "Datoteke:"

#: package/contents/ui/ConfigFilter.qml:64
#, kde-format
msgid "Show all"
msgstr "Pokaži vse"

#: package/contents/ui/ConfigFilter.qml:64
#, kde-format
msgid "Show matching"
msgstr "Pokaži ujemanja"

#: package/contents/ui/ConfigFilter.qml:64
#, kde-format
msgid "Hide matching"
msgstr "Skrij ujemanja"

#: package/contents/ui/ConfigFilter.qml:69
#, kde-format
msgid "File name pattern:"
msgstr "Vzorec imena datoteke:"

#: package/contents/ui/ConfigFilter.qml:76
#, kde-format
msgid "File types:"
msgstr "Vrste datotek:"

#: package/contents/ui/ConfigFilter.qml:82
#, kde-format
msgid "Show hidden files:"
msgstr "Pokaži skrite datoteke:"

#: package/contents/ui/ConfigFilter.qml:178
#, kde-format
msgid "File type"
msgstr "Vrsta datoteke"

#: package/contents/ui/ConfigFilter.qml:187
#, kde-format
msgid "Description"
msgstr "Opis"

#: package/contents/ui/ConfigFilter.qml:200
#, kde-format
msgid "Select All"
msgstr "Izberi vse"

#: package/contents/ui/ConfigFilter.qml:210
#, kde-format
msgid "Deselect All"
msgstr "Odstrani izbiro"

#: package/contents/ui/ConfigIcons.qml:61
#, kde-format
msgid "Panel button:"
msgstr "Gumb pulta:"

#: package/contents/ui/ConfigIcons.qml:67
#, kde-format
msgid "Use a custom icon"
msgstr "Uporabi ikono po meri"

#: package/contents/ui/ConfigIcons.qml:100
#, kde-format
msgctxt "@item:inmenu Open icon chooser dialog"
msgid "Choose…"
msgstr "Izberi…"

#: package/contents/ui/ConfigIcons.qml:106
#, kde-format
msgctxt "@item:inmenu Reset icon to default"
msgid "Clear Icon"
msgstr "Počisti ikono"

#: package/contents/ui/ConfigIcons.qml:126
#, kde-format
msgid "Arrangement:"
msgstr "Razporeditev:"

#: package/contents/ui/ConfigIcons.qml:130
#, kde-format
msgctxt "@item:inlistbox arrangement of icons"
msgid "Left to Right"
msgstr "Z leve na desno"

#: package/contents/ui/ConfigIcons.qml:131
#, kde-format
msgctxt "@item:inlistbox arrangement of icons"
msgid "Right to Left"
msgstr "Z desne na levo"

#: package/contents/ui/ConfigIcons.qml:132
#, kde-format
msgctxt "@item:inlistbox arrangement of icons"
msgid "Top to Bottom"
msgstr "Od vrha proti dnu"

#: package/contents/ui/ConfigIcons.qml:141
#, kde-format
msgctxt "@item:inlistbox alignment of icons"
msgid "Align left"
msgstr "Poravnaj levo"

#: package/contents/ui/ConfigIcons.qml:142
#, kde-format
msgctxt "@item:inlistbox alignment of icons"
msgid "Align right"
msgstr "Poravnaj desno"

#: package/contents/ui/ConfigIcons.qml:157
#, kde-format
msgid "Lock in place"
msgstr "Zakleni položaj"

#: package/contents/ui/ConfigIcons.qml:171
#, kde-format
msgid "Sorting:"
msgstr "Razvrščanje:"

#: package/contents/ui/ConfigIcons.qml:179
#, kde-format
msgctxt "@item:inlistbox sort icons manually"
msgid "Manual"
msgstr "Ročno"

#: package/contents/ui/ConfigIcons.qml:180
#, kde-format
msgctxt "@item:inlistbox sort icons by name"
msgid "Name"
msgstr "Ime"

#: package/contents/ui/ConfigIcons.qml:181
#, kde-format
msgctxt "@item:inlistbox sort icons by size"
msgid "Size"
msgstr "Velikost"

#: package/contents/ui/ConfigIcons.qml:182
#, kde-format
msgctxt "@item:inlistbox sort icons by file type"
msgid "Type"
msgstr "Vrsta"

#: package/contents/ui/ConfigIcons.qml:183
#, kde-format
msgctxt "@item:inlistbox sort icons by date"
msgid "Date"
msgstr "Datum"

#: package/contents/ui/ConfigIcons.qml:194
#, kde-format
msgctxt "@option:check sort icons in descending order"
msgid "Descending"
msgstr "Padajoče"

#: package/contents/ui/ConfigIcons.qml:202
#, kde-format
msgctxt "@option:check sort icons with folders first"
msgid "Folders first"
msgstr "Najprej mape"

#: package/contents/ui/ConfigIcons.qml:216
#, kde-format
msgctxt "whether to use icon or list view"
msgid "View mode:"
msgstr "Način prikaza:"

#: package/contents/ui/ConfigIcons.qml:218
#, kde-format
msgctxt "@item:inlistbox show icons in a list"
msgid "List"
msgstr "Seznam"

#: package/contents/ui/ConfigIcons.qml:219
#, kde-format
msgctxt "@item:inlistbox show icons in a grid"
msgid "Grid"
msgstr "Mreža"

#: package/contents/ui/ConfigIcons.qml:230
#, kde-format
msgid "Icon size:"
msgstr "Velikost ikon:"

#: package/contents/ui/ConfigIcons.qml:245
#, kde-format
msgctxt "@label:slider smallest icon size"
msgid "Small"
msgstr "Majhna"

#: package/contents/ui/ConfigIcons.qml:254
#, kde-format
msgctxt "@label:slider largest icon size"
msgid "Large"
msgstr "Velika"

#: package/contents/ui/ConfigIcons.qml:263
#, kde-format
msgid "Label width:"
msgstr "Širina značke:"

#: package/contents/ui/ConfigIcons.qml:266
#, kde-format
msgctxt "@item:inlistbox how long a text label should be"
msgid "Narrow"
msgstr "Ozka"

#: package/contents/ui/ConfigIcons.qml:267
#, kde-format
msgctxt "@item:inlistbox how long a text label should be"
msgid "Medium"
msgstr "Srednja"

#: package/contents/ui/ConfigIcons.qml:268
#, kde-format
msgctxt "@item:inlistbox how long a text label should be"
msgid "Wide"
msgstr "Široka"

#: package/contents/ui/ConfigIcons.qml:276
#, kde-format
msgid "Text lines:"
msgstr "Vrstice besedila:"

#: package/contents/ui/ConfigIcons.qml:292
#, kde-format
msgid "When hovering over icons:"
msgstr "Kadar je kazalec nad ikono:"

#: package/contents/ui/ConfigIcons.qml:294
#, kde-format
msgid "Show tooltips"
msgstr "Prikaži orodne namige"

#: package/contents/ui/ConfigIcons.qml:301
#, kde-format
msgid "Show selection markers"
msgstr "Prikaži označevalnike izbora"

#: package/contents/ui/ConfigIcons.qml:308
#, kde-format
msgid "Show folder preview popups"
msgstr "Prikaži pojavna okna s predogledom map"

#: package/contents/ui/ConfigIcons.qml:318
#, kde-format
msgid "Rename:"
msgstr "Preimenuj:"

#: package/contents/ui/ConfigIcons.qml:322
#, kde-format
msgid "Rename inline by clicking selected item's text"
msgstr "Preimenuj znotraj vrstice s klikom na izbrano besedilo postavke"

#: package/contents/ui/ConfigIcons.qml:333
#, kde-format
msgid "Previews:"
msgstr "Predogledi:"

#: package/contents/ui/ConfigIcons.qml:335
#, kde-format
msgid "Show preview thumbnails"
msgstr "Prikaži predogledne sličice"

#: package/contents/ui/ConfigIcons.qml:343
#, kde-format
msgid "Configure Preview Plugins…"
msgstr "Nastavi vtičnike za predoglede…"

#: package/contents/ui/ConfigLocation.qml:80
#, kde-format
msgid "Show:"
msgstr "Prikaži:"

#: package/contents/ui/ConfigLocation.qml:82
#, kde-format
msgid "Desktop folder"
msgstr "Mapa Namizja"

#: package/contents/ui/ConfigLocation.qml:89
#, kde-format
msgid "Files linked to the current activity"
msgstr "Datoteke povezane s trenutno dejavnostjo"

#: package/contents/ui/ConfigLocation.qml:96
#, kde-format
msgid "Places panel item:"
msgstr "Predmet panela mest:"

#: package/contents/ui/ConfigLocation.qml:129
#, kde-format
msgid "Custom location:"
msgstr "Mesta po meri:"

#: package/contents/ui/ConfigLocation.qml:137
#, kde-format
msgid "Type path or URL…"
msgstr "Vnesite pot ali naslov URL…"

#: package/contents/ui/ConfigLocation.qml:180
#, kde-format
msgid "Title:"
msgstr "Naslov:"

#: package/contents/ui/ConfigLocation.qml:182
#, kde-format
msgid "None"
msgstr "Brez"

#: package/contents/ui/ConfigLocation.qml:182
#, kde-format
msgid "Default"
msgstr "Privzeto"

#: package/contents/ui/ConfigLocation.qml:182
#, kde-format
msgid "Full path"
msgstr "Polna pot"

#: package/contents/ui/ConfigLocation.qml:182
#, kde-format
msgid "Custom title"
msgstr "Naslov po meri"

#: package/contents/ui/ConfigLocation.qml:197
#, kde-format
msgid "Enter custom title…"
msgstr "Vnesite naslov po meri…"

#: package/contents/ui/ConfigOverlay.qml:86
#, kde-format
msgid "Click and drag to rotate"
msgstr "Kliknite in povlecite za sukanje"

#: package/contents/ui/ConfigOverlay.qml:174
#, kde-format
msgid "Hide Background"
msgstr "Skrij ozadje"

#: package/contents/ui/ConfigOverlay.qml:174
#, kde-format
msgid "Show Background"
msgstr "Prikaži ozadje"

#: package/contents/ui/ConfigOverlay.qml:222
#, kde-format
msgid "Remove"
msgstr "Odstrani"

#: package/contents/ui/FolderItemPreviewPluginsDialog.qml:19
#, kde-format
msgid "Preview Plugins"
msgstr "Vstavki za predogled"

#: package/contents/ui/main.qml:375
#, kde-format
msgid "Configure Desktop and Wallpaper…"
msgstr "Nastavi namizje in ozadje…"

#: plugins/folder/directorypicker.cpp:32
#, kde-format
msgid "Select Folder"
msgstr "Izberi mapo"

#: plugins/folder/foldermodel.cpp:469
#, kde-format
msgid "&Refresh Desktop"
msgstr "Osveži namizje"

#: plugins/folder/foldermodel.cpp:469 plugins/folder/foldermodel.cpp:1625
#, kde-format
msgid "&Refresh View"
msgstr "Osveži prikaz"

#: plugins/folder/foldermodel.cpp:1634
#, kde-format
msgid "&Empty Trash"
msgstr "Izprazni Sm&eti"

#: plugins/folder/foldermodel.cpp:1637
#, kde-format
msgctxt "Restore from trash"
msgid "Restore"
msgstr "Obnovi"

#: plugins/folder/foldermodel.cpp:1640
#, kde-format
msgid "&Open"
msgstr "&Odpri"

#: plugins/folder/foldermodel.cpp:1757
#, kde-format
msgid "&Paste"
msgstr "&Prilepi"

#: plugins/folder/foldermodel.cpp:1874
#, kde-format
msgid "&Properties"
msgstr "Lastnosti"

#: plugins/folder/viewpropertiesmenu.cpp:21
#, kde-format
msgid "Sort By"
msgstr "Razvrsti po"

#: plugins/folder/viewpropertiesmenu.cpp:24
#, kde-format
msgctxt "@item:inmenu Sort icons manually"
msgid "Unsorted"
msgstr "Nerazvrščeno"

#: plugins/folder/viewpropertiesmenu.cpp:28
#, kde-format
msgctxt "@item:inmenu Sort icons by name"
msgid "Name"
msgstr "Ime"

#: plugins/folder/viewpropertiesmenu.cpp:32
#, kde-format
msgctxt "@item:inmenu Sort icons by size"
msgid "Size"
msgstr "Velikost"

#: plugins/folder/viewpropertiesmenu.cpp:36
#, kde-format
msgctxt "@item:inmenu Sort icons by file type"
msgid "Type"
msgstr "Vrsta"

#: plugins/folder/viewpropertiesmenu.cpp:40
#, kde-format
msgctxt "@item:inmenu Sort icons by date"
msgid "Date"
msgstr "Datum"

#: plugins/folder/viewpropertiesmenu.cpp:45
#, kde-format
msgctxt "@item:inmenu Sort icons in descending order"
msgid "Descending"
msgstr "Padajoče"

#: plugins/folder/viewpropertiesmenu.cpp:47
#, kde-format
msgctxt "@item:inmenu Sort icons with folders first"
msgid "Folders First"
msgstr "Najprej mape"

#: plugins/folder/viewpropertiesmenu.cpp:50
#, kde-format
msgid "Icon Size"
msgstr "Velikost ikon"

#: plugins/folder/viewpropertiesmenu.cpp:53
#, kde-format
msgctxt "@item:inmenu size of the icons"
msgid "Tiny"
msgstr "Majcena"

#: plugins/folder/viewpropertiesmenu.cpp:54
#, kde-format
msgctxt "@item:inmenu size of the icons"
msgid "Very Small"
msgstr "Zelo majhna"

#: plugins/folder/viewpropertiesmenu.cpp:55
#, kde-format
msgctxt "@item:inmenu size of the icons"
msgid "Small"
msgstr "Majhna"

#: plugins/folder/viewpropertiesmenu.cpp:56
#, kde-format
msgctxt "@item:inmenu size of the icons"
msgid "Small-Medium"
msgstr "Majhna-srednja"

#: plugins/folder/viewpropertiesmenu.cpp:57
#, kde-format
msgctxt "@item:inmenu size of the icons"
msgid "Medium"
msgstr "Srednja"

#: plugins/folder/viewpropertiesmenu.cpp:58
#, kde-format
msgctxt "@item:inmenu size of the icons"
msgid "Large"
msgstr "Velika"

#: plugins/folder/viewpropertiesmenu.cpp:59
#, kde-format
msgctxt "@item:inmenu size of the icons"
msgid "Huge"
msgstr "Ogromna"

#: plugins/folder/viewpropertiesmenu.cpp:67
#, kde-format
msgctxt "@item:inmenu arrangement of icons"
msgid "Arrange"
msgstr "Razporedi"

#: plugins/folder/viewpropertiesmenu.cpp:71
#, kde-format
msgctxt "@item:inmenu arrangement of icons"
msgid "Left to Right"
msgstr "Z leve na desno"

#: plugins/folder/viewpropertiesmenu.cpp:72
#, kde-format
msgctxt "@item:inmenu arrangement of icons"
msgid "Right to Left"
msgstr "Z desne na levo"

#: plugins/folder/viewpropertiesmenu.cpp:76
#, kde-format
msgctxt "@item:inmenu arrangement of icons"
msgid "Top to Bottom"
msgstr "Od vrha proti dnu"

#: plugins/folder/viewpropertiesmenu.cpp:81
#, kde-format
msgid "Align"
msgstr "Poravnaj"

#: plugins/folder/viewpropertiesmenu.cpp:84
#, kde-format
msgctxt "@item:inmenu alignment of icons"
msgid "Left"
msgstr "Levo"

#: plugins/folder/viewpropertiesmenu.cpp:88
#, kde-format
msgctxt "@item:inmenu alignment of icons"
msgid "Right"
msgstr "Desno"

#: plugins/folder/viewpropertiesmenu.cpp:93
#, kde-format
msgid "Show Previews"
msgstr "Pokaži predoglede"

#: plugins/folder/viewpropertiesmenu.cpp:97
#, kde-format
msgctxt "@item:inmenu lock icon positions in place"
msgid "Locked"
msgstr "Zaklenjeno"

#~ msgid "Rotate"
#~ msgstr "Zavrti"

#~ msgid "Open Externally"
#~ msgstr "Odpri od zunaj"

#~ msgid "OK"
#~ msgstr "OK"

#~ msgid "Cancel"
#~ msgstr "Prekliči"

#~ msgctxt "@item:inlistbox arrangement of icons"
#~ msgid "Rows"
#~ msgstr "Vrstic"

#~ msgctxt "@item:inlistbox arrangement of icons"
#~ msgid "Columns"
#~ msgstr "Stolpcev"

#~ msgctxt "@item:inmenu arrangement of icons"
#~ msgid "Rows"
#~ msgstr "Vrstic"

#~ msgctxt "@item:inmenu arrangement of icons"
#~ msgid "Columns"
#~ msgstr "Stolpcev"

#~ msgid "Features:"
#~ msgstr "Zmožnosti:"

#~ msgid "Search…"
#~ msgstr "Poišči…"

#~ msgid "&Create Folder"
#~ msgstr "Ustvari &mapo"

#~ msgid "Tweaks"
#~ msgstr "Prilagoditve"

#~ msgid "Show the desktop toolbox"
#~ msgstr "Pokaži namizno orodjarno"

#~ msgid "Press and hold widgets to move them and reveal their handles"
#~ msgstr ""
#~ "Kliknite in držite gradnike, da jih premaknete in odkrijete njihove ročice"

#~ msgid "Widgets unlocked"
#~ msgstr "Gradniki odklenjeni"

#~ msgid ""
#~ "You can press and hold widgets to move them and reveal their handles."
#~ msgstr ""
#~ "Gradnike lahko kliknete in držite, da jih premaknete in odkrijete njihove "
#~ "ročice."

#~ msgid "Got it"
#~ msgstr "Razumem"

#~ msgid "Resize"
#~ msgstr "Spremeni velikost"

#~ msgid "Size:"
#~ msgstr "Velikost:"

#~ msgid "Desktop Layout"
#~ msgstr "Razpored namizja"

#~ msgid "Widget Handling"
#~ msgstr "Ravnanje z gradniki"

#~ msgid "Arrange in"
#~ msgstr "Razporedi v"

#~ msgid "Sort by"
#~ msgstr "Razvrsti po"

#~ msgid "Appearance:"
#~ msgstr "Videz:"

#~ msgid "Location:"
#~ msgstr "Mesto:"

#~ msgid "Show the Desktop folder"
#~ msgstr "Pokaži vsebino mape Namizje"

#~ msgid "Specify a folder:"
#~ msgstr "Navedite mapo:"

#~ msgid "&Reload"
#~ msgstr "&Znova naloži"

#~ msgid "&Move to Trash"
#~ msgstr "Pre&makni v smeti"

#~ msgid "&Delete"
#~ msgstr "I&zbriši"

#~ msgid "Align:"
#~ msgstr "Poravnaj:"

#~ msgid "Sorting"
#~ msgstr "Razvrščanje"

#~ msgid "Up"
#~ msgstr "Gor"

#~ msgid ""
#~ "Tweaks are experimental options that may become defaults depending on "
#~ "your feedback."
#~ msgstr ""
#~ "Prilagoditve so preizkusne možnosti, ki bodo nekoč morda postale privzete "
#~ "- odvisno od vašega odziva."

#~ msgid "Show Original Directory"
#~ msgstr "Prikaži izvorno mapo"

#~ msgid "Show Original File"
#~ msgstr "Prikaži izvorno datoteko"

#~ msgid "&Configure Trash Bin"
#~ msgstr "&Nastavi Smeti"

#~ msgid "&Bookmark This Page"
#~ msgstr "&Zaznamuj to stran"

#~ msgid "&Bookmark This Location"
#~ msgstr "&Zaznamuj to mesto"

#~ msgid "&Bookmark This Folder"
#~ msgstr "&Zaznamuj to mapo"

#~ msgid "&Bookmark This Link"
#~ msgstr "&Zaznamuj to povezavo"

#~ msgid "&Bookmark This File"
#~ msgstr "&Zaznamuj to datoteko"

#~ msgctxt "@title:menu"
#~ msgid "Copy To"
#~ msgstr "Kopiraj v"

#~ msgctxt "@title:menu"
#~ msgid "Move To"
#~ msgstr "Premakni v"

#~ msgctxt "@title:menu"
#~ msgid "Home Folder"
#~ msgstr "Domača mapa"

#~ msgctxt "@title:menu"
#~ msgid "Root Folder"
#~ msgstr "Korenska mapa"

#~ msgctxt "@title:menu in Copy To or Move To submenu"
#~ msgid "Browse..."
#~ msgstr "Prebrskaj ..."

#~ msgctxt "@title:menu"
#~ msgid "Copy Here"
#~ msgstr "Kopiraj sem"

#~ msgctxt "@title:menu"
#~ msgid "Move Here"
#~ msgstr "Premakni sem"

#~ msgid "Share"
#~ msgstr "Deli"

#~ msgid "Icon:"
#~ msgstr "Ikona:"
