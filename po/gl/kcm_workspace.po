# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Marce Villarino <mvillarino@kde-espana.org>, 2014.
# Adrián Chaves (Gallaecio) <adrian@chaves.io>, 2017, 2018, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-11 02:11+0000\n"
"PO-Revision-Date: 2019-09-28 23:51+0200\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <proxecto@trasno.gal>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: ui/main.qml:23
#, kde-format
msgid ""
"The system must be restarted before changes to the middle-click paste "
"setting can take effect."
msgstr ""

#: ui/main.qml:29
#, kde-format
msgid "Restart"
msgstr ""

#: ui/main.qml:46
#, kde-format
msgid "Visual behavior:"
msgstr "Comportamento visual:"

#. i18n: ectx: label, entry (delay), group (PlasmaToolTips)
#: ui/main.qml:47 workspaceoptions_plasmasettings.kcfg:9
#, kde-format
msgid "Display informational tooltips on mouse hover"
msgstr "Amosar axudas ao cubrir co rato."

#. i18n: ectx: label, entry (osdEnabled), group (OSD)
#: ui/main.qml:58 workspaceoptions_plasmasettings.kcfg:15
#, kde-format
msgid "Display visual feedback for status changes"
msgstr "Amosar información visual sobre cambios de estado."

#: ui/main.qml:76
#, kde-format
msgid "Animation speed:"
msgstr "Velocidade das animacións:"

#: ui/main.qml:99
#, kde-format
msgctxt "Animation speed"
msgid "Slow"
msgstr "Lenta"

#: ui/main.qml:105
#, kde-format
msgctxt "Animation speed"
msgid "Instant"
msgstr "Instantánea"

#: ui/main.qml:120
#, fuzzy, kde-format
#| msgid "Single-click to open files and folders"
msgctxt ""
"part of a sentence: 'Clicking files or folders [opens them/selects them]'"
msgid "Clicking files or folders:"
msgstr "Un só clic para abrir ficheiros e cartafoles"

#: ui/main.qml:121
#, kde-format
msgctxt "part of a sentence: 'Clicking files or folders opens them'"
msgid "Opens them"
msgstr ""

#: ui/main.qml:134
#, kde-format
msgid "Select by clicking on item's selection marker"
msgstr ""

#: ui/main.qml:144
#, kde-format
msgctxt "part of a sentence: 'Clicking files or folders selects them'"
msgid "Selects them"
msgstr ""

#: ui/main.qml:158
#, kde-format
msgid "Open by double-clicking instead"
msgstr ""

#: ui/main.qml:172
#, kde-format
msgid "Clicking in scrollbar track:"
msgstr ""

#: ui/main.qml:173
#, kde-format
msgctxt ""
"@radio part of a complete sentence: 'Clicking in scrollbar track scrolls to "
"the clicked location'"
msgid "Scrolls to the clicked location"
msgstr ""

#: ui/main.qml:191
#, kde-format
msgctxt ""
"@radio part of a complete sentence: 'Clicking in scrollbar track scrolls one "
"page up or down'"
msgid "Scrolls one page up or down"
msgstr ""

#: ui/main.qml:204
#, kde-format
msgid "Middle-click to scroll to clicked location"
msgstr ""

#: ui/main.qml:216
#, kde-format
msgctxt ""
"@radio part of a complete sentence: 'Middle click pastes selected text'"
msgid "Middle Click:"
msgstr ""

#: ui/main.qml:218
#, kde-format
msgctxt ""
"@radio part of a complete sentence: 'Middle click pastes selected text'"
msgid "Pastes selected text"
msgstr ""

#: ui/main.qml:235
#, kde-format
msgid "Touch Mode:"
msgstr ""

#: ui/main.qml:237
#, kde-format
msgctxt "As in: 'Touch Mode is automatically enabled as needed'"
msgid "Automatically enable as needed"
msgstr ""

#: ui/main.qml:237 ui/main.qml:267
#, kde-format
msgctxt "As in: 'Touch Mode is never enabled'"
msgid "Never enabled"
msgstr ""

#: ui/main.qml:249
#, kde-format
msgid ""
"Touch Mode will be automatically activated whenever the system detects a "
"touchscreen but no mouse or touchpad. For example: when a transformable "
"laptop's keyboard is flipped around or detached."
msgstr ""

#: ui/main.qml:254
#, kde-format
msgctxt "As in: 'Touch Mode is always enabled'"
msgid "Always enabled"
msgstr ""

#: ui/main.qml:281
#, kde-format
msgid ""
"In Touch Mode, many elements of the user interface will become larger to "
"more easily accommodate touch interaction."
msgstr ""

#. i18n: ectx: label, entry (singleClick), group (KDE)
#: workspaceoptions_kdeglobalssettings.kcfg:9
#, fuzzy, kde-format
#| msgid "Single-click to open files and folders"
msgid "Single click to open files"
msgstr "Un só clic para abrir ficheiros e cartafoles"

#. i18n: ectx: label, entry (animationDurationFactor), group (KDE)
#: workspaceoptions_kdeglobalssettings.kcfg:13
#, fuzzy, kde-format
#| msgid "Animation speed:"
msgid "Animation speed"
msgstr "Velocidade das animacións:"

#. i18n: ectx: label, entry (scrollbarLeftClickNavigatesByPage), group (KDE)
#: workspaceoptions_kdeglobalssettings.kcfg:17
#, kde-format
msgid "Left-click in scrollbar track moves scrollbar by one page"
msgstr ""

#. i18n: ectx: label, entry (tabletMode), group (Input)
#: workspaceoptions_kwinsettings.kcfg:9
#, kde-format
msgid "Automatically switch to touch-optimized mode"
msgstr ""

#. i18n: ectx: label, entry (primarySelection), group (Wayland)
#: workspaceoptions_kwinsettings.kcfg:15
#, kde-format
msgid "Enable middle click selection pasting"
msgstr ""

#. i18n: ectx: tooltip, entry (osdEnabled), group (OSD)
#: workspaceoptions_plasmasettings.kcfg:16
#, kde-format
msgid ""
"Show an on-screen display to indicate status changes such as brightness or "
"volume"
msgstr ""

#. i18n: ectx: label, entry (osdKbdLayoutChangedEnabled), group (OSD)
#: workspaceoptions_plasmasettings.kcfg:20
#, kde-format
msgid "OSD on layout change"
msgstr ""

#. i18n: ectx: tooltip, entry (osdKbdLayoutChangedEnabled), group (OSD)
#: workspaceoptions_plasmasettings.kcfg:21
#, kde-format
msgid "Show a popup on layout changes"
msgstr ""

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Marce Villarino"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "mvillarino@kde-espana.org"

#~ msgid "General Behavior"
#~ msgstr "Comportamento xeral"

#~ msgid "System Settings module for configuring general workspace behavior."
#~ msgstr ""
#~ "Módulo de configuración do sistema para configurar o comportamento xeral "
#~ "do espazo de traballo."

#~ msgid "Furkan Tokac"
#~ msgstr "Furkan Tokac"

#~ msgid "Click behavior:"
#~ msgstr "Comportamento dos clics:"

#, fuzzy
#~| msgid "Single-click to open files and folders"
#~ msgid "Double-click to open files and folders"
#~ msgstr "Un só clic para abrir ficheiros e cartafoles"

#~ msgid "Double-click to open files and folders (single click to select)"
#~ msgstr ""
#~ "Facer duplo-clic para abrir ficheiros e cartafoles (un click para "
#~ "seleccionar)"

#~ msgid "Plasma Workspace global options"
#~ msgstr "Opcións globais do espazo de traballo de Plasma"

#~ msgid "(c) 2009 Marco Martin"
#~ msgstr "© 2009 Marco Martin"

#~ msgid "Marco Martin"
#~ msgstr "Marco Martin"

#~ msgid "Maintainer"
#~ msgstr "Mantedor"

#~ msgid "Show Informational Tips"
#~ msgstr "Mostrar consellos informativos:"
