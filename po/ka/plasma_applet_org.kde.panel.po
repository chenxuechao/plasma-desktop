# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-01 02:16+0000\n"
"PO-Revision-Date: 2022-09-02 07:48+0200\n"
"Last-Translator: Temuri Doghonadze <temuri.doghonadze@gmail.com>\n"
"Language-Team: Georgian <kde-i18n-doc@kde.org>\n"
"Language: ka\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.1.1\n"

#: contents/ui/ConfigOverlay.qml:262 contents/ui/ConfigOverlay.qml:295
#, kde-format
msgid "Remove"
msgstr "წაშლა"

#: contents/ui/ConfigOverlay.qml:272
#, kde-format
msgid "Configure…"
msgstr "მორგება…"

#: contents/ui/ConfigOverlay.qml:282
#, kde-format
msgid "Show Alternatives…"
msgstr "ალტერნატივების ჩვენება…"

#: contents/ui/ConfigOverlay.qml:305
#, kde-format
msgid "Spacer width"
msgstr "დამშორებლის სიგანე"
