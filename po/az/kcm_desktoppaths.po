# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Xəyyam Qocayev <xxmn77@gmail.com>, 2020, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-06 02:04+0000\n"
"PO-Revision-Date: 2023-02-19 16:36+0400\n"
"Last-Translator: Kheyyam <xxmn77@gmail.com>\n"
"Language-Team: Azerbaijani <kde-i18n-doc@kde.org>\n"
"Language: az\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.2\n"

#: desktoppathssettings.cpp:210
#, kde-format
msgid "Desktop"
msgstr "İş masası"

#: desktoppathssettings.cpp:225
#, kde-format
msgid "Documents"
msgstr "Sənədlər"

#: desktoppathssettings.cpp:240
#, kde-format
msgid "Downloads"
msgstr "Yükləmələr"

#: desktoppathssettings.cpp:255
#, kde-format
msgid "Music"
msgstr "Musiqilər"

#: desktoppathssettings.cpp:270
#, kde-format
msgid "Pictures"
msgstr "Şəkillər"

#: desktoppathssettings.cpp:285
#, kde-format
msgid "Videos"
msgstr "Videolar"

#: desktoppathssettings.cpp:300
#, kde-format
msgid "Public"
msgstr "İctimai"

#: desktoppathssettings.cpp:315
#, kde-format
msgid "Templates"
msgstr "Şablonlar"

#: ui/main.qml:52
#, kde-format
msgid "Desktop path:"
msgstr "İş masası yolu:"

#: ui/main.qml:58
#, kde-format
msgid ""
"This folder contains all the files which you see on your desktop. You can "
"change the location of this folder if you want to, and the contents will "
"move automatically to the new location as well."
msgstr ""
"Bu qovluq İş masasında gördüyünüz bütün fayllardan ibarətdir. İstəsəniz bu "
"qovluğun yerini dəyişdirə bilərsiniz və tərkibindəkilər avtomatik olaraq "
"yeni yerə köçəcəkdir."

#: ui/main.qml:66
#, kde-format
msgid "Documents path:"
msgstr "Sənədlər yolu:"

#: ui/main.qml:72
#, kde-format
msgid ""
"This folder will be used by default to load or save documents from or to."
msgstr ""
"Bu qovluq sənədləri yükləmək və ya saxlamaq üçün standart olaraq istifadə "
"ediləcəkdir."

#: ui/main.qml:80
#, kde-format
msgid "Downloads path:"
msgstr "Yükləmələr yolu:"

#: ui/main.qml:86
#, kde-format
msgid "This folder will be used by default to save your downloaded items."
msgstr ""
"Yüklənənləri saxlamaq üçün bu qovluq standart olaraq istifadə olunacaq."

#: ui/main.qml:94
#, kde-format
msgid "Videos path:"
msgstr "Videolar yolu:"

#: ui/main.qml:100 ui/main.qml:142
#, kde-format
msgid "This folder will be used by default to load or save movies from or to."
msgstr ""
"Bu qovluq videoları yükləmək və ya saxlamaq üçün standart olaraq istifadə "
"ediləcəkdir."

#: ui/main.qml:108
#, kde-format
msgid "Pictures path:"
msgstr "Şəkillərə yol:"

#: ui/main.qml:114
#, kde-format
msgid ""
"This folder will be used by default to load or save pictures from or to."
msgstr ""
"Bu qovluq, adətən şəkilləri yükləmək və ya saxlamaq üçün istifadə olunacaq."

#: ui/main.qml:122
#, kde-format
msgid "Music path:"
msgstr "Musiqilərə yol:"

#: ui/main.qml:128
#, kde-format
msgid "This folder will be used by default to load or save music from or to."
msgstr ""
"Bu qovluq standart olaraq musiqi yükləmək və ya saxlamaq üçün istifadə "
"olunacaq."

#: ui/main.qml:136
#, kde-format
msgid "Public path:"
msgstr "Ümumi yol:"

#: ui/main.qml:150
#, kde-format
msgid "Templates path:"
msgstr "Şablonlar yolu:"

#: ui/main.qml:156
#, kde-format
msgid ""
"This folder will be used by default to load or save templates from or to."
msgstr ""
"Bu qovluq standart olaraq şablonları yükləmək və ya saxlamaq üçün istifadə "
"olunacaq."

#: ui/UrlRequester.qml:74
#, kde-format
msgctxt "@action:button"
msgid "Choose new location"
msgstr "Yeni yer seç"

#~ msgid ""
#~ "<h1>Paths</h1>\n"
#~ "This module allows you to choose where in the filesystem the files on "
#~ "your desktop should be stored.\n"
#~ "Use the \"Whats This?\" (Shift+F1) to get help on specific options."
#~ msgstr ""
#~ "<h1>Yollar</h1>\n"
#~ "Bu modul, İş masasındakı faylların harada saxlandığını fayl sistemində "
#~ "seçməyə imkan verir.\n"
#~ "\"Bu nədir\" (Shift+F1) istifadə edərək xüsusi seçimlər haqqında məlumat "
#~ "alın."

#, fuzzy
#~| msgid ""
#~| "This folder will be used by default to load or save pictures from or to."
#~ msgid ""
#~ "This folder will be used by default to load or save public shares from or "
#~ "to."
#~ msgstr ""
#~ "Bu qovluq, adətən şəkilləri yükləmək və ya saxlamaq üçün istifadə "
#~ "olunacaq."

#~ msgid "Autostart path:"
#~ msgstr "Avtobaşlama yolu:"

#~ msgid ""
#~ "This folder contains applications or links to applications (shortcuts) "
#~ "that you want to have started automatically whenever the session starts. "
#~ "You can change the location of this folder if you want to, and the "
#~ "contents will move automatically to the new location as well."
#~ msgstr ""
#~ "Bu qovluqda sessiya başlayanda avtomatik olaraq başlatmaq istədiyiniz "
#~ "tətbiqlər və ya tətbiqlərə bağlantılar (qısa yollar) var. İstəsəniz bu "
#~ "qovluğun yerini dəyişdirə bilərsiniz və tərkibi avtomatik olaraq yeni "
#~ "yerə də köçəcəkdir."
