# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# A S Alam <alam.yellow@gmail.com>, 2019, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-01 02:16+0000\n"
"PO-Revision-Date: 2021-10-09 09:16-0700\n"
"Last-Translator: A S Alam <aalam@satluj.org>\n"
"Language-Team: Punjabi <punjabi-users@lists.sf.net>\n"
"Language: pa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.04.3\n"

#: contents/ui/ConfigOverlay.qml:262 contents/ui/ConfigOverlay.qml:295
#, kde-format
msgid "Remove"
msgstr "ਹਟਾਓ"

#: contents/ui/ConfigOverlay.qml:272
#, kde-format
msgid "Configure…"
msgstr "…ਸੰਰਚਨਾ"

#: contents/ui/ConfigOverlay.qml:282
#, kde-format
msgid "Show Alternatives…"
msgstr "…ਬਦਲ ਵੇਖਾਓ"

#: contents/ui/ConfigOverlay.qml:305
#, kde-format
msgid "Spacer width"
msgstr ""
