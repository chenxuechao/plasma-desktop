# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Zayed Al-Saidi <zayed.alsaidi@gmail.com>, 2021, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-02 02:09+0000\n"
"PO-Revision-Date: 2023-01-14 13:28+0400\n"
"Last-Translator: Zayed Al-Saidi <zayed.alsaidi@gmail.com>\n"
"Language-Team: ar\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"
"X-Generator: Lokalize 21.12.3\n"

#: globalaccelmodel.cpp:202
#, kde-format
msgctxt ""
"%1 is the name of the component, %2 is the action for which saving failed"
msgid "Error while saving shortcut %1: %2"
msgstr "خطأ أثناء الحفظ الاختصار %1: %2"

#: globalaccelmodel.cpp:314
#, kde-format
msgctxt "%1 is the name of an application"
msgid "Error while adding %1, it seems it has no actions."
msgstr "خطأ أثناء إضافة %1، لا تبدو أنها تحوي إجراءات."

#: globalaccelmodel.cpp:357
#, kde-format
msgid "Error while communicating with the global shortcuts service"
msgstr "حدث خطأ أثناء الاتصال بخدمة الاختصارات العالمية"

#: kcm_keys.cpp:57
#, kde-format
msgid "Failed to communicate with global shortcuts daemon"
msgstr "فشل الاتصال بخدمة الاختصارات العالمية"

#: kcm_keys.cpp:312
#, kde-format
msgctxt "%2 is the name of a category inside the 'Common Actions' section"
msgid ""
"Shortcut %1 is already assigned to the common %2 action '%3'.\n"
"Do you want to reassign it?"
msgstr ""
"عُين الاختصار %1 بالفعل إلى  '%3' من إجراءات %2 .\n"
"هل تريد إعادة تعيينه؟"

#: kcm_keys.cpp:316
#, kde-format
msgid ""
"Shortcut %1 is already assigned to action '%2' of %3.\n"
"Do you want to reassign it?"
msgstr ""
"عُين الاختصار %1 بالفعل إلى  '%2' من إجراءات %3. \n"
"هل تريد إعادة تعيينه؟"

#: kcm_keys.cpp:317
#, kde-format
msgctxt "@title:window"
msgid "Found conflict"
msgstr "يوجد تضارب"

#: standardshortcutsmodel.cpp:34
#, kde-format
msgid "File"
msgstr "ملف"

#: standardshortcutsmodel.cpp:35
#, kde-format
msgid "Edit"
msgstr "حرّر"

#: standardshortcutsmodel.cpp:37
#, kde-format
msgid "Navigation"
msgstr "التنقّل"

#: standardshortcutsmodel.cpp:38
#, kde-format
msgid "View"
msgstr "عرض"

#: standardshortcutsmodel.cpp:39
#, kde-format
msgid "Settings"
msgstr "الإعدادات"

#: standardshortcutsmodel.cpp:40
#, kde-format
msgid "Help"
msgstr "مساعدة"

#: ui/main.qml:27
#, kde-format
msgid "Applications"
msgstr "التّطبيقات"

#: ui/main.qml:27
#, kde-format
msgid "Commands"
msgstr "الأوامر"

#: ui/main.qml:27
#, kde-format
msgid "System Settings"
msgstr "إعدادات النّظام"

#: ui/main.qml:27
#, kde-format
msgid "Common Actions"
msgstr "إجراءات عامة"

#: ui/main.qml:51
#, kde-format
msgid "Cannot export scheme while there are unsaved changes"
msgstr "لا يمكن تصدير تشكيلة أثناء وجود تغييرات غير محفوظة"

#: ui/main.qml:63
#, kde-format
msgid ""
"Select the components below that should be included in the exported scheme"
msgstr "حدد المكونات أدناه التي يجب تضمينها في التشكيلة المصدرة"

#: ui/main.qml:69
#, kde-format
msgid "Save scheme"
msgstr "احفظ التشكيلة"

#: ui/main.qml:169
#, kde-format
msgctxt "@tooltip:button %1 is the text of a custom command"
msgid "Edit command for %1"
msgstr "حرر أمر لـ%1"

#: ui/main.qml:185
#, kde-format
msgid "Remove all shortcuts for %1"
msgstr "أزل كل اختصارات %1"

#: ui/main.qml:196
#, kde-format
msgid "Undo deletion"
msgstr "تراجع عن الحذف"

#: ui/main.qml:247
#, kde-format
msgid "No items matched the search terms"
msgstr "لا توجد عناصر مطابقة لبحثك"

#: ui/main.qml:280
#, kde-format
msgid "Select an item from the list to view its shortcuts here"
msgstr "حدد عنصرًا من القائمة لعرض اختصاراته هنا"

#: ui/main.qml:305
#, kde-format
msgctxt "@action:button Keep translated text as short as possible"
msgid "Add Application…"
msgstr "أضف تطبيق..."

#: ui/main.qml:315
#, kde-format
msgctxt "@action:button Keep translated text as short as possible"
msgid "Add Command…"
msgstr "أضف أمر…"

#: ui/main.qml:334
#, kde-format
msgid "Import Scheme…"
msgstr "استورد تشكيلة..."

#: ui/main.qml:339
#, kde-format
msgid "Cancel Export"
msgstr "ألغ التصدير"

#: ui/main.qml:339
#, kde-format
msgid "Export Scheme…"
msgstr "صدر التشكيلة..."

#: ui/main.qml:359
#, kde-format
msgid "Export Shortcut Scheme"
msgstr "صدّر تشكيلة الاختصارات"

#: ui/main.qml:359 ui/main.qml:475
#, kde-format
msgid "Import Shortcut Scheme"
msgstr "استورد تشكيلة الاختصارات"

#: ui/main.qml:361
#, kde-format
msgctxt "Template for file dialog"
msgid "Shortcut Scheme (*.kksrc)"
msgstr "تشكيلة اختصارات (*.kksrc)"

#: ui/main.qml:389
#, kde-format
msgid "Edit Command"
msgstr "حرر أمر"

#: ui/main.qml:389
#, kde-format
msgid "Add Command"
msgstr "أضف أمر"

#: ui/main.qml:407
#, kde-format
msgid "Save"
msgstr "احفظ"

#: ui/main.qml:407
#, kde-format
msgid "Add"
msgstr "أضف"

#: ui/main.qml:432
#, kde-format
msgid "Enter a command or choose a script file:"
msgstr "أدخِل أمرًا أو اختر ملف برمجي نصي:"

#: ui/main.qml:446
#, kde-format
msgctxt "@action:button"
msgid "Choose…"
msgstr "اختر..."

#: ui/main.qml:459
#, kde-format
msgctxt "@title:window"
msgid "Choose Script File"
msgstr "اختر ملف برمجي نصي"

#: ui/main.qml:461
#, kde-format
msgctxt "Template for file dialog"
msgid "Script file (*.*sh)"
msgstr "ملف برمجي (*.*sh)"

#: ui/main.qml:482
#, kde-format
msgid "Select the scheme to import:"
msgstr "اختر تشكيلة لاستيرادها:"

#: ui/main.qml:496
#, kde-format
msgid "Custom Scheme"
msgstr "تشكيلة مخصصة"

#: ui/main.qml:501
#, kde-format
msgid "Select File…"
msgstr "اختر ملف..."

#: ui/main.qml:501
#, kde-format
msgid "Import"
msgstr "استورد"

#: ui/ShortcutActionDelegate.qml:30
#, kde-format
msgid "Editing shortcut: %1"
msgstr "حرر اختصار: %1"

#: ui/ShortcutActionDelegate.qml:42
#, kde-format
msgctxt ""
"%1 is the name action that is triggered by the key sequences following "
"after :"
msgid "%1:"
msgstr "%1:"

#: ui/ShortcutActionDelegate.qml:55
#, kde-format
msgid "No active shortcuts"
msgstr "لا يوجد اختصارات نشطة"

#: ui/ShortcutActionDelegate.qml:95
#, kde-format
msgctxt "%1 decides if singular or plural will be used"
msgid "Default shortcut"
msgid_plural "Default shortcuts"
msgstr[0] "اختصار مبدئي"
msgstr[1] "اختصار مبدئي"
msgstr[2] "اختصارين مبدئيين"
msgstr[3] "اختصارات مبدئية"
msgstr[4] "اختصارا مبدئيا"
msgstr[5] "اختصار مبدئي"

#: ui/ShortcutActionDelegate.qml:97
#, kde-format
msgid "No default shortcuts"
msgstr "لا يوجد اختصارات مبدئية"

#: ui/ShortcutActionDelegate.qml:105
#, kde-format
msgid "Default shortcut %1 is enabled."
msgstr "الاختصار المبدئي %1ممكن."

#: ui/ShortcutActionDelegate.qml:105
#, kde-format
msgid "Default shortcut %1 is disabled."
msgstr "الاختصار المبدئي %1معطل."

#: ui/ShortcutActionDelegate.qml:126
#, kde-format
msgid "Custom shortcuts"
msgstr "اختصارات مخصّصة"

#: ui/ShortcutActionDelegate.qml:151
#, kde-format
msgid "Delete this shortcut"
msgstr "احذف هذا الاختصار"

#: ui/ShortcutActionDelegate.qml:157
#, kde-format
msgid "Add custom shortcut"
msgstr "أضف اختصارًا مخصصًا"

#: ui/ShortcutActionDelegate.qml:192
#, kde-format
msgid "Cancel capturing of new shortcut"
msgstr "ألغ التقاط الاختصار الجديد"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "زايد السعيدي"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "zayed.alsaidi@gmail.com"

#~ msgid "Shortcuts"
#~ msgstr "الاختصارات"

#~ msgid "David Redondo"
#~ msgstr "David Redondo"
